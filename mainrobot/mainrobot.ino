#include <Arduino.h>
#include <U8g2lib.h>
#include <Servo.h>

#define STEP_1 5
#define STEP_2 6
#define DIR_1 7
#define DIR_2 8
#define SPOS_H A0
#define RPOS_H A1
#define VOLIN A2

int m_reset_deg_1 {90};
int m_reset_deg_2 {90};

int m_volt_i;
float m_volt_f;
int m_pow;

Servo myservo1, myservo2;  // 构造舵机对象

SoftwareSerial mySerial(10, 11); // RX, TX
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ A5, /* data=*/ A4, /* reset=*/ U8X8_PIN_NONE);

int m_spd {500};  //步进电机脉冲间隔
int m_soft_spd {10};
float m_position_g {0};   //g轴电机转动的圈数
float m_position_h {0};   //h轴电机转动的圈数
int m_pulse_g {0};    //g轴所需要的脉冲
int m_pulse_h {0};    //h轴所需要的脉冲

String m_command_in;

void setup() {
  Serial.begin(115200);   //打开主串口
  mySerial.begin(9600);   //打开软串口
  u8g2.begin();
  myservo1.attach(3);
  myservo2.attach(4);
  PinInit();
  ResetPosition();    //舵机及步进电机归位

}

void loop() {
  GetControlWord();   //获取串口信息

  DataParse(m_command_in);    //解析串口控制数据

  GetVoltage();

  ReturnMess();

}

void GetControlWord()
{
  while (mySerial.available()) {  //串口读取到的转发到wifi，因为串口是一位一位的发送所以在这里缓存完再发送
    char m_chr;
    m_chr = char(mySerial.read());
    if (m_chr == '\n') break;
    m_command_in += m_chr;
    delay(2);
  }
}

//数据解析函数
void DataParse(String m_command)
{
  if (m_command.substring(0, 4) == "MAIN")
  {
    String m_model, m_data;
    int adr = 1;
    m_model = m_command.substring(4, 5);
    Serial.print("model :");
    Serial.print(m_model);
    m_data = m_command.substring(4, m_command.indexOf("$") - 1);
    Serial.print(" data :");
    // DoMotion();    //正式使用时将此函数解注释
  }

}

void DoMotion(String m_model, String m_data)
{
    int m_work_data = m_data.toInt();
    if(m_model == "A") ServoAWork(m_work_data);
    else if(m_model == "B") ServoBWork(m_work_data);
    else if(m_model == "G") DoM1Pulse(m_work_data / 1000);    //为方便传输与计算，步进电机的位置与角度都乘以一千发送
    else if(m_model == "H") DoM2Pulse(m_work_data / 1000);
}

void ServoAWork(int m_deg)
{
  m_deg = DegLimit(m_deg);
  myservo1.write(m_deg);
  delay(1000);
}

void ServoBWork(int m_deg)
{
  m_deg = DegLimit(m_deg);
  myservo2.write(m_deg);
  delay(1000);
}

void DegLimit(int m_deg)
{
  if(m_deg > 180) m_deg = 180;
  else if(m_deg < 0) m_deg = 0;
  return m_deg;
}

void PinInit()
{
  pinMode(STEP_1, OUTPUT);
  pinMode(STEP_2, OUTPUT);
  pinMode(DIR_1, OUTPUT);
  pinMode(DIR_2, OUTPUT);
}

void DoM1Pulse(float m_pos)
{
  m_pulse_g = 3400 * abs(m_pos - m_position_g);
  if(m_pos - m_position_g > 0)
  {
    analogWrite(DIR_1, 255);
  }
  else{
    analogWrite(DIR_1, 0);
  }
  Moter1Work(m_pulse_g);
}

void DoM2Pulse(float m_pos)
{
  m_pulse_h = 3400 * abs(m_pos - m_position_h);
  if(m_pos - m_position_h > 0)
  {
    analogWrite(DIR_2, 255);
  }
  else{
    analogWrite(DIR_2, 0);
  }
  Moter1Work(m_pulse_h);
}

void Moter1Work(int m_cyc_num)
{
  for(int i = 0; i < m_cyc_num; i++)
  {
    analogWrite(STEP_1, 1023);
    delayMicroseconds(m_spd);
    analogWrite(STEP_1, 0);
    delayMicroseconds(m_spd);
  }
  delay(1000);
}

void Moter2Work(int m_cyc_num)
{
  for(int i = 0; i < m_cyc_num; i++)
  {
    analogWrite(STEP_2, 1023);
    delayMicroseconds(m_spd);
    analogWrite(STEP_2, 0);
    delayMicroseconds(m_spd);
  }
    delay(1000);
}

void ResetPosition()
{
  while(analogRead(SPOS_H) > 500)
  {
    analogWrite(STEP_1, 1023);
    delay(m_soft_spd);
    analogWrite(STEP_1, 0);
    delay(m_soft_spd);
  }
  myservo1.write(m_reset_deg_1);
  delay(1000);
  myservo2.write(m_reset_deg_2);
  delay(1000);
}

void GetVoltage()
{
  m_volt_i = map(analogRead(VOLIN), 0, 1023, 0, 3500);
  m_volt_f = m_volt_i / 100;
  if(m_volt_i > 2400)
  {
    m_volt_i = 2400;
  }
  else if(m_volt_i < 2200)
  {
    m_volt_i = 2200;
  }
  m_pow = map(m_volt_i, 2200, 2400, 0, 100);
}

void ReturnMess()
{
  mySerial.print("P");
  mySerial.println(m_pow);
}

void DispIPMess()
{
  u8g2.setCursor(0,15);
  u8g2.print("");
}